package com.ejhobbs.aestheticboard;

import android.annotation.TargetApi;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Build;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputConnection;

import java.util.concurrent.ThreadLocalRandom;

public class Wideboard extends InputMethodService
    implements KeyboardView.OnKeyboardActionListener {
    private KeyboardView view;
    private Keyboard board;
    private boolean upper = false;

    @Override
    public View onCreateInputView() {
        view = (KeyboardView) getLayoutInflater()
                .inflate(R.layout.keyboard, null);
        board = new Keyboard(this, R.xml.qwerty);
        view.setKeyboard(board);
        view.setOnKeyboardActionListener(this);
        return view;
    }

    @Override
    public void onPress(int primaryCode) {

    }

    @Override
    public void onRelease(int primaryCode) {

    }

    @Override
    public void onKey(int key, int[] keyCodes) {
        InputConnection ic = getCurrentInputConnection();
        switch(key) {
            case Keyboard.KEYCODE_DELETE:
                ic.deleteSurroundingText(1,0);
                break;
            case Keyboard.KEYCODE_SHIFT:
                upper = !upper;
                board.setShifted(upper);
                view.invalidateAllKeys();
                break;
            case Keyboard.KEYCODE_DONE:
                ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                break;
            case Keyboard.KEYCODE_ALT:
                ic.commitText(String.valueOf(getRandomKata()), 1);
                break;
            default:
                ic.commitText(String.valueOf(getWideChar((char)key)), 1);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private char getRandomKata() {
        ThreadLocalRandom r = ThreadLocalRandom.current();
        int res = r.nextInt(0x30A0, 0x30FF+1);
        return (char) res;
    }

    /*
    Thank you to vape for this quick translation method
    https://github.com/joshuarli/vape/blob/master/src/main.rs
     */
    private char getWideChar(char key) {
        if (key == 0x0020) {
            return (char) 0x3000;
        } else {
            char wideKey = key;
            if (key >= 0x0021 && key <= 0x007e) {
                wideKey += 0xfee0;
            }
            return wideKey;
        }
    }

    @Override
    public void onText(CharSequence text) {

    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }
}
